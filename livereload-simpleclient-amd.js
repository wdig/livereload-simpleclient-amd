/**
 * simplistic Livereload UMD Module
 */
(function (root, factory) {
	if (typeof define === 'function' && define.amd) {
		define([], factory);
	} else if (typeof module === 'object' && module.exports) {
		module.exports = factory();
	} else {
		root.returnExports = factory();
	}
}(this, function () {
	"use strict";

	var connection = { 'hostname': '', 'port': ''},
		cookieName = 'livereloadsimpleclientamd',
		insertScript, checkUrlParam, checkLiveReloadCookie;
	/**
	 * Private Methods
	 */

	/**
	 * Inserts the livereload Script into DOM
	 */
	insertScript = function() {
		var	head = document.getElementsByTagName("head")[0],
			sTag = document.createElement("script");
		sTag.defer = true;
		sTag.type = "text/javascript";
		sTag.src = "http://"+connection.hostname+':'+connection.port+'/livereload.js';
		head.appendChild(sTag);
		return true;
	};

	/**
	 * check for URL-Param to activate Livereload
	 *
	 * sets a cookie accordingly
	 */
	checkUrlParam = function() {
		var re, matches;
		re = /\?\w*livereload=(on|off)/gi;
		matches = re.exec(window.location.search);
		if (matches === null) {
			return;
		} else if (matches[1] === 'on') {
			if (!checkLiveReloadCookie()) {
				document.cookie = cookieName+'=on';
			}
		} else if (matches[1] === 'off') {
			document.cookie = cookieName+"=; expires=Thu, 01 Jan 1970 00:00:00 GMT;";
		}
	};

	/**
	 * check for our livereload Cookie
	 * returns boolean
	 */
	checkLiveReloadCookie = function() {
		return (document.cookie.match(new RegExp(cookieName + '=([^;]+)','gi'))) ? true:false;
	};

	/**
	 * Public Methods
	 */

	return {
		/**
		 * init
		 *
		 * @param {string} hostRe suitable as Regular Expression to match against location.hostname.
		 *                        If matched, livereload is used.
		 * @param {hash} connection Hash with "hostname" and "port" of the livereload server
		 * @return {boolean} success
		 */
		init: function(hostRe, conn) {
			// nonmatching hostname, return
			if (! window.location.hostname.match(new RegExp(hostRe,'gi'))) {
				return false;
			}
			connection.hostname = conn.hostname;
			connection.port = conn.port;
			// check for url-param to activate or deactivate
			checkUrlParam();
			if (checkLiveReloadCookie()) {
				return insertScript();
			}
			return false;
		}
	};

}));
