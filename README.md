livereload-simpleclient-amd
=======================

This is a very simple livereload script, meant to be loaded through [require.js](http://requirejs.org/) as AMD Module.
It also works with [JSPM](http://jspm.io/) / [SystemJS](https://github.com/systemjs/systemjs).

It's intended to be used in rather simple environments, that don't have a build-process that would remove the livereload client from your sources and being active on a production server.

The downside is, that the script is always loaded from the server, issuing a additional HTTP-Request. 

## Usage ##
Call method init() with two parameters.
First parameter is a String to be used as a Regular Expression to match against a window.location.hostname. Livereload can only be enabled on matching hosts.
The second parameter is a hash with hostname and port of the livereload server.

To activate livereload pass *livereload=on* as URL-Parameter. The script will then set a session-cookie, enabling livereload on subsequent requests.
To deactivate it, pass *livereload=off*.

## Example ##

Using RequireJS
```
#!javascript
requirejs(["livereload-simpleclient-amd"], function (lsa) {
    "use strict";
    lsa.init("my\.super\.host\.name", {
        "hostname": "livereload.host.name", 
        "port": 9875
    });
});
```

Or using SystemJS (after installing via JSPM)
```
#!javascript
import lsa from 'livereload-simpleclient-amd';
lsa.init('my\.super\.host\.name', { 'hostname': 'livereload.host.name', 'port': 9875 });

```